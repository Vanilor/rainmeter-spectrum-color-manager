/*
 *  Created by Vanilor
 *
 */

#include <stdio.h>

int main() {
	
	FILE* f = fopen("../config.txt", "w+");
	unsigned short linesNb = 127, red = 252, green = 0, blue = 0, alpha = 225;
	float angle = -7.3f;
	//PhaseThresold : 21
	
	for(int i = 1; i < linesNb; i++){
		
		if(i <= 21)
			blue += 12;
		else if(i <= 42)
			red -= 12;
		else if(i <= 63)
			green += 12;
		else if(i <= 84)
			blue -= 12;
		else if(i <= 105)
			red += 12;
		else if(i <= 126)
			green -= 12;
		
		angle -= 0.05f;
		
		fprintf(f, ""
		 "[r%d]\n"
   "Meter=ROUNDLINE\n"
   "w=(670/0.744)\n"
   "h=(670/0.744)\n"
   "LineWidth=(670/55)\n"
   "StartAngle=%.2f\n"
   "LineLength=(670/2.136)\n"
   "LineStart=((670/2.136)+([mr%d]*(670/8.375)))\n"
 "LineColor=%d,%d,%d,%d\n"
 "DynamicVariables=1\n"
 "AntiAlias=1\n\n", i, angle, i, red, blue, green, alpha);
	
	}
	
	return 0;
}
